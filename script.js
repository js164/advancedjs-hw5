class Card {
    constructor(title, textContent, fullName, userName, email, id) {
        this.title = title;
        this.textContent = textContent;
        this.fullName = fullName;
        this.email = email;
        this.id = id;
    }

    renderCard() {
        const cardLayout = `
    <div class="card" data-id="${this.id}">
        <div>${this.fullName} ${this.email}</div>
        <button class="button"> X </button>
        <div>${this.title}</div>
        <div>${this.textContent}</div>
    </div>
    `;
        wrapper.insertAdjacentHTML('afterbegin', cardLayout);
    }
}


const wrapper = document.querySelector('.container')

async function getCards() {
    try {
        const resUsers = await fetch('https://ajax.test-danit.com/api/json/users')
        const users = await resUsers.json()

        const resPosts = await fetch('https://ajax.test-danit.com/api/json/posts')
        const posts = await resPosts.json()

        users.forEach((user) => {
            posts.forEach((post) => {
                if (user.id === post.userId) {
                    const card = new Card(post.title, post.body, user.name, user.username, user.email, post.id)
                    card.renderCard();
                }
                const button = document.querySelector('.button')
                button.addEventListener('click', del)
            });
        });
    }
    catch (error) {
        console.log(error)
    }
}

async function del(e) {
    const elem = e.target.closest('.card')
try{
    await fetch(`https://ajax.test-danit.com/api/json/posts/${elem.dataset.id}`, { method: 'DELETE' }).then((res) => {
        if (res.status === 200 || res.ok === true) {
            elem.remove()
        }
    })
}
catch (error) {
    console.log(error)
}

}

getCards()
